/*
 *   Lantern - Turn on the lights
 *   Copyright (C) WinX64 2017
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package io.github.winx64.lantern.command;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffectType;

import io.github.winx64.lantern.Lantern;
import io.github.winx64.lantern.LanternMode;
import io.github.winx64.lantern.Util;
import io.github.winx64.lantern.configuration.LanternMessages.Message;

public final class CommandLantern implements TabExecutor {

	private final Lantern plugin;

	public CommandLantern(Lantern plugin) {
		this.plugin = plugin;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command, String alias, String[] args) {
		if (!(sender instanceof Player)) {
			sender.sendMessage(plugin.getMessages().get(Message.COMMAND_NO_CONSOLE));
			return true;
		}

		Player player = (Player) sender;
		LanternMode mode = args.length < 1 ? LanternMode.AUTO : LanternMode.getLanternMode(args[0]);
		if (mode == null) {
			sender.sendMessage(plugin.getMessages().get(Message.COMMAND_LANTERN_INVALID_MODE, args[0]));
			return true;
		}

		LanternMode currentMode = plugin.getPlayerLanternMode(player);
		if (mode == currentMode) {
			sender.sendMessage(plugin.getMessages().get(Message.COMMAND_LANTERN_MODE_ALREADY_SET, mode));
			return true;
		}

		plugin.setPlayerLanternMode(player, mode);
		player.sendMessage(plugin.getMessages().get(Message.COMMAND_LANTERN_MODE_SET, mode));
		if (mode == LanternMode.ON) {
			player.addPotionEffect(Util.LANTERN);
		} else if (mode == LanternMode.OFF) {
			player.removePotionEffect(PotionEffectType.NIGHT_VISION);
		}
		return true;
	}

	@Override
	public List<String> onTabComplete(CommandSender sender, Command command, String alias, String[] args) {
		if (!(sender instanceof Player)) {
			return Util.EMPTY_LIST;
		}

		String match = args[0].toLowerCase();
		List<String> matches = new ArrayList<String>();

		for (LanternMode mode : LanternMode.values()) {
			if (mode.name().toLowerCase().startsWith(match)) {
				matches.add(mode.name().toLowerCase());
			}
		}

		return matches;
	}
}
