/*
 *   Lantern - Turn on the lights
 *   Copyright (C) WinX64 2017
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package io.github.winx64.lantern;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.potion.PotionEffectType;

import io.github.winx64.lantern.command.CommandLantern;
import io.github.winx64.lantern.command.CommandReload;
import io.github.winx64.lantern.configuration.LanternMessages;
import io.github.winx64.lantern.listener.PlayerListener;

public final class Lantern extends JavaPlugin {

	private final Logger logger;
	private final LanternMessages messages;

	private final Map<UUID, LanternMode> lanternMode;

	public Lantern() {
		this.logger = getLogger();
		this.messages = new LanternMessages(this);

		this.lanternMode = new HashMap<UUID, LanternMode>();
	}

	@Override
	public void onEnable() {

		if (!messages.loadMessages()) {
			halt("Configuration load error");
			return;
		}

		if (!registerCommands()) {
			halt("Command registration error");
			return;
		}

		Bukkit.getPluginManager().registerEvents(new PlayerListener(this), this);

		for (Player player : Bukkit.getOnlinePlayers()) {
			this.lanternMode.put(player.getUniqueId(), LanternMode.OFF);
		}
	}

	@Override
	public void onDisable() {
		for (Player player : Bukkit.getOnlinePlayers()) {
			LanternMode currentMode = this.lanternMode.get(player.getUniqueId());
			if (currentMode != LanternMode.OFF) {
				player.removePotionEffect(PotionEffectType.NIGHT_VISION);
			}
		}
	}

	private boolean registerCommands() {
		try {
			getCommand("lantern").setExecutor(new CommandLantern(this));
			getCommand("lantern-reload").setExecutor(new CommandReload(this));
			return true;
		} catch (Exception e) {
			log(Level.SEVERE, e, "An error occurred while trying to register the commands! Details below:");
			return false;
		}
	}

	private void halt(String reason) {
		log(Level.SEVERE, "The plugin will now be disabled! Reason: %s", reason);
		Bukkit.getPluginManager().disablePlugin(this);
	}

	public LanternMessages getMessages() {
		return messages;
	}

	public LanternMode getPlayerLanternMode(Player player) {
		return lanternMode.get(player.getUniqueId());
	}

	public void setPlayerLanternMode(Player player, LanternMode mode) {
		this.lanternMode.put(player.getUniqueId(), mode);
	}

	public LanternMode removePlayerLanternMode(Player player) {
		return lanternMode.remove(player.getUniqueId());
	}

	public void log(String format, Object... args) {
		log(Level.INFO, null, String.format(format, args));
	}

	public void log(String message) {
		log(Level.INFO, null, message);
	}

	public void log(Level level, String format, Object... args) {
		log(level, null, String.format(format, args));
	}

	public void log(Level level, Exception e, String format, Object... args) {
		log(level, e, String.format(format, args));
	}

	public void log(Level level, String message) {
		log(level, null, message);
	}

	public void log(Level level, Exception e, String message) {
		logger.log(level, message, e);
	}
}
