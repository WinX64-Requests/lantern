/*
 *   Lantern - Turn on the lights
 *   Copyright (C) WinX64 2017
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package io.github.winx64.lantern.listener;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.potion.PotionEffectType;

import io.github.winx64.lantern.Lantern;
import io.github.winx64.lantern.LanternMode;
import io.github.winx64.lantern.Util;

public final class PlayerListener implements Listener {

	private static final int DARKNESS_THRESHOLD = 7;

	private final Lantern plugin;

	public PlayerListener(Lantern plugin) {
		this.plugin = plugin;
	}

	@EventHandler
	public void onJoin(PlayerJoinEvent event) {
		plugin.setPlayerLanternMode(event.getPlayer(), LanternMode.OFF);
	}

	@EventHandler
	public void onMove(PlayerMoveEvent event) {
		Player player = event.getPlayer();
		if (plugin.getPlayerLanternMode(player) == LanternMode.AUTO) {
			int lightLevel = player.getEyeLocation().getBlock().getLightLevel();
			if (lightLevel <= DARKNESS_THRESHOLD && !player.hasPotionEffect(PotionEffectType.NIGHT_VISION)) {
				player.addPotionEffect(Util.LANTERN);
			} else if (lightLevel > DARKNESS_THRESHOLD && player.hasPotionEffect(PotionEffectType.NIGHT_VISION)) {
				player.removePotionEffect(PotionEffectType.NIGHT_VISION);
			}
		}
	}

	@EventHandler
	public void onQuit(PlayerQuitEvent event) {
		Player player = event.getPlayer();
		LanternMode currentMode = plugin.removePlayerLanternMode(player);
		if (currentMode != LanternMode.OFF) {
			player.removePotionEffect(PotionEffectType.NIGHT_VISION);
		}
	}
}
